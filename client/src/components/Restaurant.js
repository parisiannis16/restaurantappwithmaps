import React, { Component } from "react";
import Marker from "./Marker";

class Restaurant extends Component {
  render() {
    const google = window.google;
    const positionCurrent = {
      latitude: this.props.latitude,
      longitude: this.props.longitude
    };
    // return new google.maps.Marker((position = { positionCurrent }));
    return new google.maps.marker({
      position: positionCurrent,
      map: this.props.map
    });
  }
}

export default Restaurant;
