import {
  Navbar,
  Nav,
  NavDropdown,
  MenuItem,
  Form,
  FormGroup,
  FormControl,
  Button
} from "react-bootstrap";
import React from "react";
import ReactDOM from "react-dom";

const navBarStyle = {
  borderRadius: "0px",
  marginBottom: "0px"
};

const inputStyle = {
  marginTop: "7px"
};

export class Navigatie extends React.Component {
  render() {
    return (
      <div>
        <Navbar style={navBarStyle} inverse fixedTop>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#brand">FoodMap-Bucuresti</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>

          <Navbar.Collapse>
            <Nav>
              <NavDropdown eventKey={2} title="Cuisine" id="basic-nav-dropdown">
                <MenuItem eventKey={2.1}>Romanian</MenuItem>
                <MenuItem eventKey={2.1}>International</MenuItem>
                <MenuItem eventKey={2.1}>Bistros</MenuItem>
                <MenuItem eventKey={2.1}>Cafes</MenuItem>
              </NavDropdown>
              <NavDropdown eventKey={1} title="Price" id="basic-nav-dropdown">
                <MenuItem eventKey={1.1}>$$$$ (very expensive)</MenuItem>
                <MenuItem eventKey={1.2}>$$$ (expensive)</MenuItem>
                <MenuItem eventKey={1.3}>$$ (moderate)</MenuItem>
                <MenuItem eventKey={1.4}>$ (inexpensive)</MenuItem>
              </NavDropdown>

              <NavDropdown eventKey={3} title="Rating" id="basic-nav-dropdown">
                <MenuItem eventKey={3.1}>★★★★ (Extraordinary)</MenuItem>
                <MenuItem eventKey={3.2}>★★★ (Excellent)</MenuItem>
                <MenuItem eventKey={3.3}>★★ (Very Good)</MenuItem>
                <MenuItem eventKey={3.4}>★ (Good)</MenuItem>
              </NavDropdown>
            </Nav>

            <Nav pullRight>
              {/* <NavItem eventKey={1} href="#"> */}
              <div class="input" style={inputStyle}>
                <Form inline>
                  <FormGroup controlId="SearchArea">
                    <FormControl type="search" placeholder="McDonalds..." />
                  </FormGroup>{" "}
                  <Button type="submit" bsStyle="info">
                    Search
                  </Button>
                </Form>
                {/* </NavItem> */}
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Navigatie;
