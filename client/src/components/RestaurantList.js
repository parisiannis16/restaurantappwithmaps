import React, { Component } from "react";
import RestaurantStore from "../stores/RestaurantStore";
import Restaurant from "./Restaurant";

// import StudentForm from './StudentForm'

class RestaurantList extends Component {
  constructor() {
    super();
    this.store = new RestaurantStore();
    this.state = {
      restaurants: []
    };
    this.add = restaurant => {
      this.store.addOne(restaurant);
    };
    this.map = this.props.map;
  }
  componentDidMount() {
    this.store.getAll();
    this.store.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        restaurants: this.store.content
      });
    });
  }
  render() {
    return this.state.restaurants.map((e, i) => (
      <Restaurant item={e} key={i} />
    ));
  }
}

export default RestaurantList;
