import React, { Component } from "react";
import ContactBody from "./ContactBody";
import RestaurantList from "./components/RestaurantList";

class App extends Component {
  render() {
    return (
      <div>
        <ContactBody>
          <RestaurantList />
        </ContactBody>
      </div>
    );
  }
}

export default App;
