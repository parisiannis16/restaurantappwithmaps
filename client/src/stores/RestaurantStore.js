import axios from 'axios'
import { EventEmitter } from 'fbemitter'

const SERVER = 'http://localhost:8080'

class RestaurantStore {
    constructor() {
        this.content = []
        this.emitter = new EventEmitter()
    }
    async getAll() {
        try {
            let response = await axios(`${SERVER}/restaurants`)
            this.content = response.data
            this.emitter.emit('GET_ALL_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('GET_ALL_ERROR')
        }
    }
    async addOne(student) {
        try {
            await axios.post(`${SERVER}/restaurants`, student)
            this.emitter.emit('ADD_SUCCESS')
            this.getAll()
        } catch (e) {
            console.warn(e)
            this.emitter.emit('ADD_ERROR')
        }
    }
}

export default RestaurantStore