"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const Cors = require("cors");

const sequelize = new Sequelize("restaurants", "root", "admin", {
  dialect: "mysql",
  define: {
    timestamps: false
  }
});

const Restaurant = sequelize.define("restaurant", {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [3, 255]
    }
  },
  price: {
    type: Sequelize.FLOAT,
    allowNull: false,
    validate: {
      len: [3, 255]
    }
  },
  rating: {
    type: Sequelize.FLOAT,
    allowNull: false,
    validate: {
      len: [3, 255]
    }
  },
  latitude: {
    type: Sequelize.FLOAT,
    allowNull: false,
    vvalidate: {
      len: [3, 255]
    }
  },
  longitude: {
    type: Sequelize.FLOAT,
    allowNull: false,
    validate: {
      len: [3, 255]
    }
  }
});

const app = express();
app.use(bodyParser.json());
app.use(express.static("../client/build"));

app.get("/create", async (req, res) => {
  try {
    await sequelize.sync({ force: true });
    res.status(201).json({ message: "created" });
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.get("/restaurants", async (req, res) => {
  try {
    let restaurant;
    if (req.query && req.query.filter) {
      restaurant = await Restaurant.findAll({
        where: {
          name: {
            [Op.like]: `%${req.query.filter}%`
          }
        }
      });
    } else {
      restaurant = await Restaurant.findAll();
    }
    res.status(200).json(restaurant);
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.post("/restaurants", async (req, res) => {
  try {
    if (req.query.bulk && req.query.bulk == "on") {
      await Restaurant.bulkCreate(req.body);
      res.status(201).json({ message: "created" });
    } else {
      await Restaurant.create(req.body);
      res.status(201).json({ message: "created" });
    }
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.get("/restaurants/:id", async (req, res) => {
  try {
    let restaurant = await Restaurant.findById(req.params.id);
    if (restaurant) {
      res.status(200).json(restaurant);
    } else {
      res.status(404).json({ message: "not found" });
    }
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.put("/restaurants/:id", async (req, res) => {
  try {
    let restaurant = await Restaurant.findById(req.params.id);
    if (restaurant) {
      await restaurant.update(req.body);
      res.status(202).json({ message: "accepted" });
    } else {
      res.status(404).json({ message: "not found" });
    }
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.delete("/restaurants/:id", async (req, res) => {
  try {
    let restaurant = await Restaurant.findById(req.params.id);
    if (restaurant) {
      await restaurant.destroy();
      res.status(202).json({ message: "accepted" });
    } else {
      res.status(404).json({ message: "not found" });
    }
  } catch (e) {
    console.warn(e);
    res.status(500).json({ message: "server error" });
  }
});

app.listen(8080);
